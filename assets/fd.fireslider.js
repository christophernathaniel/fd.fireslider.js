// Minamalistic JS Slider
// Author: Christopher Nathaniel Seaman
// URI: https://firedeveloper.co.uk
// Bitbucket : https://bitbucket.org/christophernathaniel/fd.fireslider.js/src/master/

let initSlider = function(slideID, setTime) {
    let _this = this;
    _this.selectUnit = document.getElementById(slideID);
    let i; let pos = 0;
    const selectItem = _this.selectUnit.querySelectorAll('.item');
    const slideWidth = (selectItem.length * 100);
    const coitemWidth = (slideWidth * 100) / slideWidth;
    _this.selectUnit.setAttribute("style", "width:" + slideWidth + "%");

    for(x = 0; x < selectItem.length; x++) {
      selectItem[x].setAttribute("style", "width:" + 100 / selectItem.length + "%");
    };

    setInterval(function(){
      pos = pos + coitemWidth;
      _this.selectUnit.setAttribute('style', 'left: -' + (pos - 100) + '%; ' +
                                    'width:' + slideWidth + '%;');
      if (pos >= slideWidth) pos = 0;
    }, setTime);
}

// let fireSlider = new initSlider('sliderunit');
